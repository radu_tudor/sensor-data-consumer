#SENSOR DATA CONSUMER

##Prerequisites
1. go in the `sensor-data` project
2. build it using maven (`mvn clean package` or `./mvnw clean package` or `mvnw.cmd clean package`)
3. after it is built, run it with `java -jar target/sensor-data.jar`
4. using **curl**, **postman** or **your browser of choice**, preform some `GET` requests on:
- http://localhost:8080/data-sets/0
- http://localhost:8080/data-sets/1
- http://localhost:8080/data-sets/2
- http://localhost:8080/data-sets/3

##Requirements
Write a analytics service that pulls data from the `senor-data` service over HTTP and 
does the following:
- given a single data set id (like the 0,1,2,3 above) it can determine the average temperature, 
humidity and co2. For each of these measurements (temperature, humidity, co2) it should also 
determine the most recent value that had a deviation of more than 10% from the average
- given a set of ids, it can determine which data set has the largest plant height. 
For this data set it should also determine the steepest growth interval 
(between which two consecutive temporal data points did the plant height grow the most)